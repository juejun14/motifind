import collections
import copy
import time

import Bio.SeqFeature
import pandas as pd

from find_motif import *
from Bio import SwissProt
import warnings

warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)


class FindDomain(Motif):
    """
    This script is used  to generate structural information of motif result,
    It will return two excel tables,
    one is about the global domain result, another is about the details.
    """
    def __init__(self):
        Motif.__init__(self)
        self.not_interest = []

    def import_database(self, filepath, motif_dict=None):
        """
        Import the database by a local file and generate a dictionary to store information
        :param filepath: the absolute path of database file (type: .txt)
        :param motif_dict: Select the information by keys
        :return: a dictionary containing the accession number and feature information
        """
        self.handle = open(filepath, 'r')
        self.feature_dict = collections.defaultdict(list)

        for record in SwissProt.parse(self.handle):
            # Summary dictionary for all feature information
            if not motif_dict:
                self.feature_dict[record.accessions[0]] = record.features
            if motif_dict:
                for key, value in motif_dict.items():
                    if record.accessions[0] in key:
                        self.feature_dict[record.accessions[0]] = record.features
        return self.feature_dict

    def choose_category(self, category):
        # Define the unimportant notion of feature table
        self.category = category

        # Customize to define the uninteresting region
        all_notion = ['ACT_SITE', 'BINDING', 'CARBOHYD', 'CA_BIND', 'CHAIN', 'COILED', 'COMPBIAS', 'CONFLICT',
                      'CROSSLNK', 'DISULFID', 'DNA_BIND', 'DOMAIN', 'INIT_MET', 'INTRAMEM', 'HELIX', 'LIPID',
                      'METAL', 'MOD_RES', 'MOTIF', 'MUTAGEN', 'NON_CONS', 'NON_STD', 'NON_TER', 'NP_BIND',
                      'PEPTIDE', 'PROPEP', 'REGION', 'REPEAT', 'SIGNAL', 'SITE', 'STRAND', 'TOPO_DOM', 'TRANSIT',
                      'TRANSMEM', 'TURN', 'UNSURE', 'VARIANT', 'VAR_SEQ', 'ZN_FING']
        for i in self.category:
            all_notion.remove(i)
        self.not_interest = all_notion
        return self.not_interest

    def create_zone_dictionary(self, structure_dict, keyword=None):
        """
        Store structured and unstructured area information separately

        :param structure_dict: input the summary feature dictionary
        :param keyword: input the name of motif to avoid motif annotated in the structured dictionary
        :return: 4 different dictionaries:
                - 2 for the structured domain and its qualifiers
                - 2 for the unstructured domain and its qualifiers
        """

        # 4 dictionary to separate structural domain and others
        self.zone_function_dict = collections.defaultdict(list)
        self.zone_non_function_dict = collections.defaultdict(list)
        self.qualifiers_function_dict = collections.defaultdict(list)
        self.qualifiers_non_function_dict = collections.defaultdict(list)

        for key, value in structure_dict.items():
            for l in value:
                # Skip uninteresting areas
                if l.type in self.not_interest:
                    pass
                elif l.type == 'REGION':
                    # Add Disordered region in to the unstructured dictionary
                    if 'Disordered' in l.qualifiers['note']:
                        self.zone_non_function_dict[key].append(l.location)
                        self.qualifiers_non_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                    # Avoid the interest motif has been annotated
                    elif keyword:
                        if keyword in l.qualifiers['note']:
                            self.zone_non_function_dict[key].append(l.location)
                            self.qualifiers_non_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                        else:
                            self.qualifiers_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                            self.zone_function_dict[key].append(l.location)
                    # Add others in to the structured dictionary
                    else:
                        self.qualifiers_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                        self.zone_function_dict[key].append(l.location)
                elif l.type == 'MOTIF':
                    # Avoid the interest motif has been annotated
                    if keyword:
                        if keyword in l.qualifiers['note']:
                            self.zone_non_function_dict[key].append(l.location)
                            self.qualifiers_non_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                        else:
                            self.qualifiers_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                            self.zone_function_dict[key].append(l.location)
                    else:
                        self.qualifiers_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                        self.zone_function_dict[key].append(l.location)
                elif l.type == 'TOPO_DOM':
                    if 'Cytoplasmic' in l.qualifiers['note']:
                        self.zone_non_function_dict[key].append(l.location)
                        self.qualifiers_non_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                    else:
                        self.qualifiers_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                        self.zone_function_dict[key].append(l.location)
                else:
                    self.qualifiers_function_dict[key].append((str(l.location), l.type, l.qualifiers))
                    self.zone_function_dict[key].append(l.location)

        return self.zone_function_dict, self.zone_non_function_dict, self.qualifiers_function_dict, self.qualifiers_non_function_dict

    def find_domain(self, motif_dict, zone_dict):
        """
        Compare each motif and its sequence to find out if the motif exists in a known functional structure,
        and store the results to two different dictionaries.

        :param motif_dict: the dictionary with motif and its location information
        :param zone_dict:  the dictionary with sequence domain information
        :return: Two dictionary with the motif found or not found in sequence domain
        """
        # Create two empty dictionary to stock the domain information of motif
        start = time.process_time()

        self.motif_in_domain = collections.defaultdict(list)
        self.motif_not_domain = collections.defaultdict(list)
        self.domain_qualifier = collections.defaultdict(list)
        self.function_domain_motif_nb = collections.defaultdict(list)

        for k, v in motif_dict.items():
            # If exists multiple motifs in sequence
            if type(v) == list:
                # For each motif of sequence
                for l in v:
                    # Transfer the location information to a list type
                    if type(l) == tuple:
                        # Save the start location and the end location of the motif
                        location = list(l)[0]
                        location_end = list(l)[1]
                        if k in zone_dict.keys():
                            # Compare each start location with the sequence structure dictionary
                            for element in zone_dict[k]:
                                # Avoid the unknown region
                                if type(element.end) == Bio.SeqFeature.UnknownPosition:
                                    pass
                                elif type(element.start) == Bio.SeqFeature.UnknownPosition:
                                    pass
                                # Verified if the start of motif is at the functional domain.
                                elif location in element:
                                    self.motif_in_domain[k].append((v[v.index(l)-1], location, str(element)))
                                    # Insert correspond qualifier of this pattern
                                    self.domain_qualifier[k].append([i for i in self.qualifiers_function_dict[k] if str(element) in i])
                                    self.function_domain_motif_nb[k].append(location)
                                # Verified if the end of motif is at the functional region.
                                elif location not in element and location_end in element:
                                    self.motif_in_domain[k].append((v[v.index(l)-1], location, str(element)))
                                    # Insert correspond qualifier of this pattern
                                    self.domain_qualifier[k].append([i for i in self.qualifiers_function_dict[k] if str(element) in i])
                                    self.function_domain_motif_nb[k].append(location)
                                else:
                                    pass

        # Save the motif not in domain to another dictionary
        for k, v in self.function_domain_motif_nb.items():
            self.function_domain_motif_nb[k] = list(set(v))
        for k, v in motif_dict.items():
            if type(v) == list:
                # For each motif of sequence
                for l in v:
                    # Transfer the location information to a list type
                    if type(l) == tuple:
                        # Save the start location of the motif
                        location = list(l)[0]
                        if location not in self.function_domain_motif_nb[k]:
                            # if k not in self.motif_not_domain.keys():
                            self.motif_not_domain[k].append((v[v.index(l) - 1], v[v.index(l)]))

        end = time.process_time()
        print('Find domain: ', end-start)
        return self.motif_in_domain, self.motif_not_domain

    def create_structure_df(self):
        """
        Create dataframe for structural information
        """
        description = {}
        self.domain_motif_nb = {}
        start = time.process_time()

        # Unstructured
        # Create the description dictionary by protein
        for k, v in self.prot_name_list.items():
            if k in self.motif_not_domain.keys():
                description[k] = v

        # Create dataframe to store all structural results
        self.unstructured_df = pd.DataFrame(columns=('Protein', 'Description', 'Motif', 'num_motif'))
        self.unstructured_df['Protein'] = self.motif_not_domain.keys()
        self.unstructured_df.set_index('Protein', inplace=True)
        # Add description and motif column into dataframe
        for k, v in description.items():
            self.unstructured_df['Description'][k] = v
            self.unstructured_df['Motif'][k] = self.motif_not_domain[k]
        # Add motif number column into dataframe
        for key, value in self.motif_not_domain.items():
            self.domain_motif_nb[key] = len(value)
            self.unstructured_df['num_motif'][key] = self.domain_motif_nb[key]
        # Create another dataframe with qualifier information
        self.qualifier_df = pd.DataFrame(columns=('Protein', 'Qualifiers'))
        self.qualifier_df['Protein'] = self.qualifiers_non_function_dict.keys()
        self.qualifier_df.set_index('Protein', inplace=True)
        self.qualifier_df['Qualifiers'] = self.qualifiers_non_function_dict.values()
        # Merge two dataframe into one dataframe
        self.unstructured_df = self.unstructured_df.join(self.qualifier_df, how='left')

        # Structured
        # Create the description dictionary by protein
        for k, v in self.prot_name_list.items():
            if k in self.motif_in_domain.keys():
                description[k] = v
        # Create dataframe to store all structural results
        self.struct_df = pd.DataFrame(columns=('Protein', 'Description', 'Motif', 'num_motif'))
        self.struct_df['Protein'] = self.motif_in_domain.keys()
        self.struct_df.set_index('Protein', inplace=True)
        # Add description and motif column into dataframe
        for k, v in description.items():
            self.struct_df['Description'][k] = v
            self.struct_df['Motif'][k] = self.motif_in_domain[k]
        # Add motif number column into dataframe
        for key, value in self.function_domain_motif_nb.items():
            value = list(set(value))
            self.domain_motif_nb[key] = len(value)
            self.struct_df['num_motif'][key] = self.domain_motif_nb[key]
        # Create another dataframe with qualifier information
        self.qualifier_df = pd.DataFrame(columns=('Protein', 'Qualifiers'))
        self.qualifier_df['Protein'] = self.domain_qualifier.keys()
        self.qualifier_df.set_index('Protein', inplace=True)
        self.qualifier_df['Qualifiers'] = self.domain_qualifier.values()
        # Merge two dataframe into one dataframe
        self.struct_df = self.struct_df.join(self.qualifier_df, how='left')

        end = time.process_time()
        print('Structural domain: ', end-start)
        return self.struct_df, self.unstructured_df

    def create_detail_df(self, length_dict, motif_dict, notion_list):
        """ Function to create the detail dataframe of structural information """
        start = time.process_time()

        self.detail_df = pd.DataFrame(columns=('Protein', 'Motif', 'Domain', 'Detail',
                                               'Domain Proportion', 'Structured'),
                                      index=[])
        self.detail_df.index = np.arange(1, len(self.detail_df))
        notion = notion_list
        notion = notion + ['Others', 'nb_motif']
        self.summary_df = pd.DataFrame(columns=['Protein'], index=[])

        # Create a motif dictionary copy to store the domain info
        # notion_dict = self.to_df
        notion_dict = copy.deepcopy(motif_dict)

        # temporary list for the protein have two regions
        tmp_list1 = []
        tmp_dict = collections.defaultdict(list)

        for k, v in self.motif_in_domain.items():
            for element in v:
                if type(element) == tuple:
                    # If protein in the domain dictionary
                    for value in self.domain_qualifier[k]:
                        for x in value:
                            location = list(x)[0]
                            domain = list(x)[1]
                            detail = list(x)[2]
                            try:
                                start = int(location.split(':')[0].replace('[', ''))
                                end = int(location.split(':')[1].replace(']', ''))
                                domain_length = end - start + 1
                                if element[2] == location:
                                    if element not in tmp_list1:
                                        element_list = [k, element, domain, detail, domain_length / length_dict[k],
                                                        1]
                                        index_size = self.detail_df.index.size + 1
                                        self.detail_df.loc[index_size] = element_list
                                        # Store motifs with concept (notion) information
                                        tmp_list1.append(element)
                                        tmp_dict[k].append((element[0], element[1], domain))

                            except ValueError:
                                pass
        # Drop the redundant annotation of the same notion for each motif
        for k,v in tmp_dict.items():
            tmp_dict[k] = set(v)

        for k,v in tmp_dict.items():
            for element in v:
                motif_location = notion_dict[k].index(element[0])
                if type(notion_dict[k][motif_location+1]) == str:
                    notion_dict[k].insert(motif_location, notion_dict[k][motif_location])
                    notion_dict[k].insert(motif_location + 1, element[2])
                else:
                    notion_dict[k][motif_location+1] = element[2]

        # Replace the unstructured location by keyword 'Others'
        for k, v in notion_dict.items():
            if not v:
                pass
            elif v:
                for element in v:
                    if not element:
                        pass
                    elif type(element) == tuple:
                        motif_location = notion_dict[k].index(element)
                        notion_dict[k][motif_location] = 'Others'

        self.summary_df['Protein'] = notion_dict.keys()
        self.summary_df.set_index('Protein', inplace=True)
        for i in notion:
            self.summary_df[i] = None

        for k, v in notion_dict.items():
            if not v:
                pass
            else:
                for i in notion:
                    if i in v:
                        self.summary_df.at[k, i] = v.count(i)

        # Insert total number of motif into dataframe
        nb_motif_dict = self.nb_motif_dico
        for k, v in nb_motif_dict.items():
            if k in notion_dict.keys():
                self.summary_df.at[k, 'nb_motif'] = v
        self.summary_df.fillna(0, inplace=True)

        # Add column for type of motif of protein domain: structured, unstructured or mixed
        self.summary_df['domain_type'] = ''
        for protein in motif_dict.keys():
            if protein in self.motif_not_domain.keys():
                if self.motif_in_domain[protein] == []:
                    self.summary_df.at[protein, 'domain_type'] = 'unstructured'
                elif self.motif_in_domain[protein] != []:
                    self.summary_df.at[protein, 'domain_type'] = 'mixed'
            elif protein in self.motif_in_domain.keys() and self.motif_in_domain[protein] != []:
                self.summary_df.at[protein, 'domain_type'] = 'structured'
        # print(self.summary_df)
        for k, v in self.motif_not_domain.items():
            for element in v:
                element_list = [k, element, None, None, None, 0]
                index_size = self.detail_df.index.size + 1
                self.detail_df.loc[index_size] = element_list

        end = time.process_time()
        print('Detail info: ', end-start)
        return self.detail_df, self.summary_df

    def export_struct(self, filename, df1, df2, df3=None, df4=None):
        if filename:
            df1.to_excel(filename, index=True)
            with pd.ExcelWriter(filename) as writer:
                df1.to_excel(writer, sheet_name='main_structured_table')
                df2.to_excel(writer, sheet_name='main_unstructured_table')
                if df3 is not None:
                    df3.to_excel(writer, sheet_name='detail_table')
                    df4.to_excel(writer, sheet_name='summary_motif')
            self.save = 'ok'
        else:
            self.save = 'no'



