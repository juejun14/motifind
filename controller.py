# -*- coding: utf-8 -*-
# @Time : 2022/5/1 20:16
# @Author : Juejun CHEN


from view import *
from find_motif import *
from Structure_find import *
from sub_motif import *
from find_multiple_motif import *


class Controller:
    """
    Class to manage the view script and the model script from a controller
    """
    def __init__(self):
        """ Initialization of controller class """
        self.view = View(self)
        self.model = Motif()
        self.structure = FindDomain()
        self.sub = FindSupMotif()
        self.multiple = FindMultipleMotif()
        self.pre_treat = 1
        self.radiobutton = 1
        self.program = 'find_motif'
        self.motif = ''
        self.save = ''

    def start_view(self):
        """
        Start window
        """
        self.view.main()

    def press_radio_button(self, button):
        """ Return the variable of radiobutton"""
        if button == 'Equal' or button == 'Based on data':
            self.radiobutton = self.view.filter0.get()
        elif button == 'Protein results with motif found' or button == 'All protein results':
            self.pre_treat = self.view.filter1.get()

    def press_execute(self):
        """ Execute de program for correspond program"""
        # self.view.loading_window()
        check_result = self.view.check_input()
        if check_result is False:
            return False
        else:
            if self.program == 'find_motif':
                result = self.call_find_motif()
                if result is False:
                    return False
                else:
                    self.view.start_compute()
                    self.view.result_frame()

            elif self.program == 'find_sub_motif':
                result = self.call_find_sub_motif()
                if result is False:
                    return False
                else:
                    self.view.start_compute()
                    self.view.result_frame()

            elif self.program == 'find_multiple_motif':
                result = self.call_find_multiple_motif()
                if result is False:
                    return False
                else:
                    self.view.start_compute()
                    self.view.result_frame()
            else:
                pass

    def call_find_motif(self):
        # Store the interesting motif and the fasta file path
        self.motif = self.view.input_motif.get()
        self.path_result = self.view.combobox_file()

        # When choose only fasta file
        if type(self.path_result) == str:
            self.fasta_path = self.path_result
        # When choose two file at a time
        elif type(self.path_result) == tuple:
            self.fasta_path, self.struct_path = self.view.combobox_file()
        self.model = Motif()

        if not self.path_result or not self.motif:
            self.view.show_error()
            return False
        elif self.fasta_path:
            self.dico, self.length = self.model.readfile(self.fasta_path, self.radiobutton)
            if self.dico is not False:
                self.model.find_motif(motif=self.motif, dictionary=self.dico)
                self.model.computer_proba(self.radiobutton)
                self.model.create_ratio()
                self.model.correction_log()
                self.df = self.model.create_dataframe()
                self.df = self.model.insert_to_df('ratio', self.model.ratio)
                # choose show result without 0
                if self.pre_treat == 0:
                    self.df = self.model.delete_zero()
                else:
                    pass
                try:
                    self.stats, self.chi2, self.freq = self.model.stats_table()
                except:
                    # When 0 motif found
                    self.stats, self.chi2, self.freq = None, None, self.model.stats_table()
                # When choose domain result
                if self.view.domain_table.get() == 1:
                    self.check_list = self.view.send_checkbutton()
                    if not self.struct_path:
                        self.view.show_error()
                        return False
                    elif self.check_list == []:
                        self.view.show_error()
                        return False
                    elif self.struct_path:
                        keyword = self.view.keyword.get()
                        print('keyword', keyword)
                        motif_dico = self.df['Motif'].to_dict()

                        self.structure = FindDomain()
                        self.dico, self.length = self.structure.readfile(self.fasta_path, self.radiobutton)
                        self.structure.find_motif(self.motif, self.dico)
                        feature = self.structure.import_database(self.struct_path, motif_dico)
                        # self.check_list = self.view.send_checkbutton()
                        self.structure.choose_category(self.check_list)
                        struct, unstruct, ft_struct, ft_unstruct = self.structure.create_zone_dictionary(feature,
                                                                                                         keyword=keyword)
                        self.struct_domain, self.unstruct_domain = self.structure.find_domain(motif_dico, struct)
                        self.df1, self.df2 = self.structure.create_structure_df()
                        self.detail, self.summary = self.structure.create_detail_df(self.length, motif_dico,
                                                                                    self.check_list)
        else:
            return False

    def call_find_sub_motif(self):
        self.motif = self.view.input_motif.get()
        # print(self.motif)
        self.path_result = self.view.combobox_file()
        # print(self.path_result)
        self.fasta_path = self.path_result
        self.sub_motif = self.view.input_sub_motif.get()
        # print(self.sub_motif)

        distance = self.view.distance_2_motif.get().replace(' ', '')
        self.num_invalid = ''
        if not self.motif or not self.path_result:
            self.view.show_error()
            return False
        if not self.sub_motif:
            self.view.show_error()
            return False
        else:
            # When define distance between 2 motifs
            if distance:
                print(distance)
                if distance.isdigit():
                    distance = int(distance)
                    self.sub = FindSupMotif()
                    self.prot_dico, length = self.sub.readfile(self.fasta_path)
                    self.sub.find_main_motif(self.motif, self.prot_dico)
                    self.sub_df = self.sub.sup_motif(second_motif=self.sub_motif, zone_length=distance)
                else:
                    self.num_invalid = 'yes'
                    self.view.show_error()
            # When not define distance between two motifs
            elif not distance:
                print('not')
                self.sub = FindSupMotif()
                self.prot_dico, length = self.sub.readfile(self.fasta_path)
                self.sub.find_main_motif(self.motif, self.prot_dico)
                self.sub_df = self.sub.sup_motif(self.sub_motif)

            else:
                return False

    def call_find_multiple_motif(self):
        self.motif = self.view.input_motif.get()
        self.path_result = self.view.combobox_file()

        if self.path_result:
            self.fasta_path = self.path_result

        if not self.path_result or not self.motif:
            self.view.show_error()
            return False
        else:
            motif_list = self.multiple.multiple_motif(self.motif)
            self.prot_dico, length = self.multiple.readfile(self.fasta_path)
            motif_dico, nb = self.multiple.find_motif(motif_list[0], self.prot_dico)
            result_dico, nb_dico = self.multiple.find_each_motif(motif_list, self.prot_dico)
            self.multiple_df1, self.multiple_df2 = self.multiple.create_df(result_dico, nb_dico)
            print(self.multiple_df1)


    def press_export_base(self, df=None, stats=None, chi2=None, freq=None):
        """
        Export the result of motif research
        """
        self.program = 'find_motif'
        self.save_file = self.view.export()
        if not self.save_file:
            return False
        elif self.program == 'find_motif':
            self.df = df
            self.stats = stats
            self.chi2 = chi2
            self.freq = freq
            self.model.export(filename=self.view.save_file.name, frame=self.program,
                              df=self.df, stats=self.stats, chi2=self.chi2, freq=self.freq)
            self.save = self.model.save

        self.view.show_msg()

    def press_export_detail(self, df1, df2, df3, df4):
        """
        Export the result of structural information
        """
        self.program = 'find_motif'
        self.save_file = self.view.export()
        if not self.save_file:
            return False
        elif self.program == 'find_motif':
            self.df1 = df1
            self.df2 = df2
            self.df3 = df3
            self.df4 = df4
            self.structure.export_struct(filename=self.view.save_file.name, df1=self.df1,
                                         df2=self.df2, df3=self.df3, df4=self.df4)

            self.save = self.structure.save
        self.view.show_msg()

    def press_export_sub_motif(self, df):
        """
        function to export the result when run the program 'find sub motif'
        """
        self.program = 'find_sub_motif'
        self.save_file = self.view.export()
        if not self.save_file:
            return False
        elif self.program == 'find_sub_motif':
            sub_df = df
            self.sub.export_df(filename=self.view.save_file.name, df=sub_df)

            self.save = self.sub.save

        self.view.show_msg()

    def press_export_multiple_motif(self, df1, df2):
        """
        function to export the result when run the program 'find multiple motif'
        """
        self.program = 'find_multiple_motif'
        self.save_file = self.view.export()
        if not self.save_file:
            return False
        elif self.program == 'find_multiple_motif':
            multi_df1 = df1
            multi_df2 = df2
            self.multiple.export_df(filename=self.view.save_file.name, df1=multi_df1, df2=multi_df2)

            self.save = self.multiple.save

        self.view.show_msg()

    def press_import(self):
        """ Import file """
        self.view.import_file()

    def choose_prog(self, label_id):
        """ Manage click events """
        print("[Controller][button_press_handle] " + label_id)
        if label_id == 'Find motif':
            self.view.find_motif_frame()
            self.program = 'find_motif'
        elif label_id == 'Homepage':
            self.view.reset_parameter()
            self.program = 'homepage'
            self.view.create_homepage()
        elif label_id == 'Find sub motif':
            self.view.reset_parameter()
            self.program = 'find_sub_motif'
            self.view.find_sub_motif_frame()
        elif label_id == 'Find multiple motifs':
            self.view.reset_parameter()
            self.program = 'find_multiple_motif'
            self.view.find_multiple_motif_frame()

