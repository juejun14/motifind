# MotiFind

Interface program to find the specified motif of protein sequence.

## Table of contents

* [Description](#Description)
* [Architecture](#Architecture)
* [Visuals](#Visuals)
* [Installation](#Installation)
* [Usage](#Usage)
* [Support](#Support)
* [Authors](#Authors)
* [License](#License)

## Description <a name="Description"></a>

This application was developed for an internship during Master 1 Bioinformatics.
It used to study the specified motif from the fasta file. 

Developed on windows, it is recommended to run in windows environment.  

## Architecture <a name="Architecture"></a>

The program is based on the MVC architecture.  
![image](https://gitlab.com/juejun14/motifind/-/blob/main/image/MVC.png)
#### Scripts  
There are all the scripts used for research :
* main : lance the application
* view : the GUI of application 
* controller : control the view and model script
* find_motif : main model to find the specified motif
* Structure_find : the script depended on the main model (find_motif) to do region research 
* sub_motif: Search for second motif B from the defined length succeeding motif A found
* find_multiple_motif : Search multiple topics at once
* database_research : the supplementary script used to import Excel file (not used on application)

#### UML diagram
![image](https://gitlab.com/juejun14/motifind/-/blob/main/image/UML.png)


## Visuals <a name="Visuals"></a>
![image](https://gitlab.com/juejun14/motifind/-/blob/main/image/application.png)

## Installation <a name="Installation"></a>
The program is based on python 3.10 version.

To use this program correctly, Please download all the python file from the document.  
You need to install biopython library to use this program correctly, you can install it from the command line :

````shell
pip install biopython
````

## Usage <a name="Usage"></a>
To run this program:

* Example for Windows 10:

````commandline
python main.py
````

* Example for Linux:

`````bash
python3 main.py
`````

After calling the application window, you can import the database file to test.  

The fasta file to test is in data_to_test document.  

The motif to research show be wrote in uppercase letter and use a 'X' for unknown letter of motif. 

For example: RXXL.  

And then select the interest fasta file from the combobox, click on execute.    

The result will be added to history which is at the right of application.  

You can export the result from history.  

## Support <a name="Support"></a>
If you have any question or if you want to report a bug, please contact: juejun.chen@etu.univ-amu.fr

## Roadmap <a name="Roadmap"></a>
### To do list:  

- [x] Find multiple motif at a time.
- [x] Finds if a second motif exists within a specified distance after found the first motif

## Authors <a name="Authors"></a>
CHEN Juejun

## License <a name="License"></a>
Open source projects.
