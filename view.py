# -*- coding: utf-8 -*-
# @Time : 2022/5/1 20:15
# @Author : Juejun CHEN
import sys
import tkinter
import webbrowser
from tkinter import Tk, Label, filedialog, Button, Frame, Entry, LabelFrame, ttk, Canvas
from tkinter import StringVar, IntVar
from tkinter import Radiobutton, Checkbutton, Message, Toplevel
from tkinter import messagebox, Scrollbar
from controller import *
from threading import Thread


class View:
    """ Class to create the interface """
    def __init__(self, controller):
        """
        Initialisation of interface
        """
        self.controller = controller
        self.window = Tk()
        self.window.title('MotiFind (version 2.5)')
        self.import_path = ''
        self.save_file = ''
        self.n = 0
        self.data_name = []
        self.path_dict = {}
        self.program = ''

    def main(self):
        """
        Function to create attributes of windows
        """
        self.window.configure(bg='lightsteelblue')
        # Show the window at the center of window
        w = self.window.winfo_screenwidth()
        h = self.window.winfo_screenheight()
        center_window(self.window, w, h-100)
        # Create the main frame of windows
        self.main_frame()
        self.create_program_menu()

        self.window.mainloop()

    def main_frame(self):
        """ Create the home page """
        title_frame = Frame(self.window, width=1100, height=100)
        title_frame.pack(fill='both')
        self.program_title = StringVar()
        self.program_title.set('Motif research tool')
        title = Label(title_frame, textvariable=self.program_title, bg='midnightblue',
                      fg='white', font=('microsoft yahei', 27, 'bold'))
        title.pack(fill='both')

        large_frame = Frame(self.window, bg='lightsteelblue')
        large_frame.pack(fill='both', expand=1)

        self.program_frame = LabelFrame(large_frame, text='Program', bg='steelblue', width=200, height=500,
                                        fg='white', font=('microsoft yahei', 18, 'bold'))
        self.program_frame.pack(side='left', padx=10, pady=15, fill='both')
        self.program_frame.pack_propagate(0)

        self.parameter_frame = LabelFrame(large_frame, text='Homepage', bg='steelblue', width=700, height=500,
                                          fg='white', font=('microsoft yahei', 18, 'bold'))
        self.parameter_frame.pack(side='left', padx=15, pady=15, fill='both', expand=1)
        # # Create a new frame inside of parameter frame to generate different program by choice
        self.parameter_childframe = Frame(self.parameter_frame, bg='steelblue', width=700, height=620)
        self.parameter_childframe.pack(fill='both')
        self.parameter_childframe.pack_propagate(0)

        self.create_homepage()

        self.history_frame = LabelFrame(large_frame, text='History', bg='steelblue', width=210, height=500,
                                        fg='white', font=('microsoft yahei', 18, 'bold'))
        self.history_frame.pack(side='left', padx=10, pady=15, fill='both')

        # Create import and deleted buttons
        button_frame = Frame(self.history_frame, bg='steelblue', width=200)
        button_frame.pack(pady=5)
        self.import_button = Button(button_frame, text='Import', bg='lightsteelblue', width=10,
                                    font=('microsoft yahei', 10, 'normal'),
                                    command=lambda: self.controller.press_import())
        self.import_button.pack(side='left', padx=5)

        self.import_button = Button(button_frame, text='Clear all', bg='lightsteelblue', width=10,
                                    font=('microsoft yahei', 10, 'normal'),
                                    command=lambda: self.reset_history())
        self.import_button.pack(side='left', padx=5)

        self.history_child_frame = Frame(self.history_frame, bg='steelblue', width=210, height=400)
        self.history_child_frame.pack(fill='both', expand=1)

        self.canvas = Canvas(self.history_child_frame, bg='steelblue', width=210, height=400)
        self.canvas.pack(side='left', fill='both', expand=1)

        scroll = Scrollbar(self.history_child_frame, orient='vertical', command=self.canvas.yview)
        scroll.pack(side='right', fill='y')
        self.canvas.config(yscrollcommand=scroll.set)
        self.canvas.bind('<Configure>', lambda e: self.canvas.configure(scrollregion=self.canvas.bbox("all")))

        self.document_tmp = Frame(self.canvas, bg='steelblue', width=210, height=400)
        self.canvas.create_window((0, 0), window=self.document_tmp, anchor='nw')

        author = Frame(self.window)
        author.pack()
        author_name = Label(author, text='Writed by Juejun CHEN', bg='lightsteelblue')
        author_name.pack(side='right')

    def create_homepage(self):
        """ Create the homepage """
        self.program_title.set('Motif research tool')
        self.parameter_frame['text'] = 'Homepage'
        context_frame = Frame(self.parameter_childframe, bg='steelblue',)
        context_frame.pack(side='top', fill='both', pady=10)

        title = Label(context_frame, text='Welcome to the MotiFind !',
                       bg='steelblue', font=('microsoft yahei', 25, 'bold', 'underline'))
        title.pack(pady=20)

        intro1 = "This is a program to find the specified motif in the peptide sequence. " \
                 "\nYou can import the fasta file or the text file from the button of right to create your history."
        Message(context_frame, text=intro1, justify='center', bg='steelblue',
                width=820, font=('microsoft yahei', 13, 'normal')).pack(fill='both', pady=10)

        intro2 = "To find the specified motif, please use uppercase letter and use 'X' for unknown letter of motif."
        Message(context_frame, text=intro2, justify='center', bg='white',
                width=820, font=('microsoft yahei', 13, 'bold')).pack(fill='both', pady=10)

        contact = 'If you have any question or you want to report a bug, ' \
                  '\n please click here to check the code or contact the developer'
        contact_info = Label(self.parameter_childframe, text=contact,
                             bg='steelblue', font=('microsoft yahei', 13, 'normal'))
        contact_info.bind("<Button-1>", open_url)
        contact_info.pack(side='bottom', fill='x')

    def create_program_menu(self):
        """
        Create the program frame
        """
        prog_name = ['Homepage', 'Find motif', 'Find sub motif', 'Find multiple motifs']
        n = 0
        for label in prog_name:
            lbl = Button(self.program_frame, text='-'+label,
                        bg='steelblue', relief='flat',
                        font=('microsoft yahei', 10, 'bold', 'underline'),
                        command=lambda button=label: self.controller.choose_prog(button))
            lbl.grid(row=n, sticky='w', pady=10, padx=5)
            n = n+1

    def reset_parameter(self):
        """
        Reset the parameter frame
        """
        try:
            self.parameter_childframe.destroy()
        except:
            pass
        self.parameter_childframe = Frame(self.parameter_frame, bg='steelblue', width=700, height=620)
        self.parameter_childframe.pack(fill='both')
        self.parameter_childframe.pack_propagate(0)

    def find_motif_frame(self):
        """
        Define the motif research program frame
        """
        # Reset the frame
        self.parameter_frame['text'] = 'Parameters'
        self.reset_parameter()
        self.program = 'find_motif'
        self.program_title.set('Find motif')
        # Create the new attributes
        find_motif = Frame(self.parameter_childframe, bg='steelblue', width=700, height=400)
        find_motif.pack(fill='both')

        Label1 = Label(find_motif, text='Please entry your motif :', bg='steelblue',
                       font=('microsoft yahei', 12, 'normal'))
        Label1.grid(row=0, column=0, sticky='w', pady=5, padx=10)

        self.input_motif = StringVar()
        entry_motif = Entry(find_motif, textvariable=self.input_motif, bg='white', width=62)
        entry_motif.grid(row=0, column=1, pady=5, padx=10)

        Label2 = Label(find_motif, text='Please choose the fasta file:', bg='steelblue',
                       font=('microsoft yahei', 12, 'normal'))
        Label2.grid(row=1, column=0, sticky='w', pady=5, padx=10)
        # Create the combobox to choose fasta file
        self.file_list = ttk.Combobox(find_motif, width=60, state='readonly')
        self.file_list['values'] = [x for x in self.data_name if '.fasta' in x]
        self.file_list.bind("<<ComboboxSelected>>", lambda e: self.combobox_file())
        self.file_list.grid(row=1, column=1)
        # Add option of result
        self.frame_amico_acid()
        self.frame_store_0()
        self.add_domain_info()
        # Button variable used for loading window
        self.button = 'execute'
        Bt = Button(self.parameter_childframe, text='Execute', bg='lightsteelblue', width=15,
                    font=('microsoft yahei', 12, 'normal'),
                    command=lambda: [self.set_button_variable('execute'),
                                     self.loading_window(self.controller.press_execute)])

        Bt.pack(pady=5)

    def frame_amico_acid(self):
        """
        Create the frame to define the type of frequency
        """
        freq_zone = LabelFrame(self.parameter_childframe, text='Amino acid frequency',
                               bg='steelblue',
                               font=('microsoft yahei', 12, 'normal'))
        freq_zone.pack(fill='both', pady=5)

        self.filter0 = IntVar()
        self.filter0.set(1)
        equal = Radiobutton(freq_zone, text='Equal', bg='steelblue',
                            variable=self.filter0, value=0,
                            font=('microsoft yahei', 12, 'normal'),
                            command=lambda: self.controller.press_radio_button(equal['text']))
        equal.grid(row=0, column=0, padx=10, pady=10)
        unequal = Radiobutton(freq_zone,
                              text='Based on data (Frequency = number of each amino acid / total number of sequence)',
                              bg='steelblue',
                              variable=self.filter0, value=1,
                              font=('microsoft yahei', 12, 'normal'),
                              command=lambda: self.controller.press_radio_button(unequal['text']))
        unequal.grid(row=0, column=1, padx=10, pady=5)

    def frame_store_0(self):
        """
        Create the frame to define store the 0 motif result or not
        """
        pre_treat_zone = LabelFrame(self.parameter_childframe, text='Pre treatment',
                                    bg='steelblue',
                                    font=('microsoft yahei', 12, 'normal'))
        pre_treat_zone.pack(fill='both', pady=5)

        self.filter1 = IntVar()
        self.filter1.set(1)
        without_zero = Radiobutton(pre_treat_zone, text='Protein results with motif found', bg='steelblue',
                                   variable=self.filter1, value=0,
                                   font=('microsoft yahei', 12, 'normal'),
                                   command=lambda: self.controller.press_radio_button(without_zero['text']))
        without_zero.grid(row=0, column=0, padx=10, pady=5)
        with_zero = Radiobutton(pre_treat_zone, text='All protein results', bg='steelblue',
                                variable=self.filter1, value=1,
                                font=('microsoft yahei', 12, 'normal'),
                                command=lambda: self.controller.press_radio_button(with_zero['text']))
        with_zero.grid(row=0, column=1, padx=10, pady=5)

    def add_domain_info(self):
        """
        Create option to add structural information
        """
        self.domain_zone = LabelFrame(self.parameter_childframe, text='Structure info',
                                 bg='steelblue',
                                 font=('microsoft yahei', 12, 'normal'))
        self.domain_zone.pack(fill='both', pady=5)

        self.domain_table = IntVar()
        self.domain_table.set(0)
        domain_radiobutton = Frame(self.domain_zone, bg='steelblue')
        domain_radiobutton.grid(row=0, column=0, sticky='w')
        domain_table = Radiobutton(domain_radiobutton, text='Without domain info', bg='steelblue',
                                   variable=self.domain_table, value=0,
                                   font=('microsoft yahei', 12, 'normal'),
                                   command=lambda: self.struct_option())
        domain_table.grid(row=0, column=0, padx=10, pady=5)
        not_domain_table = Radiobutton(domain_radiobutton, text='With domain info', bg='steelblue',
                                       variable=self.domain_table, value=1,
                                       font=('microsoft yahei', 12, 'normal'),
                                       command=lambda: self.struct_option())
        not_domain_table.grid(row=0, column=1, padx=10, pady=5)

    def struct_option(self):
        if self.domain_table.get() == 0:
            try:
                self.option_frame.grid_forget()
            except AttributeError:
                pass

        elif self.domain_table.get() == 1:
            self.option_frame = Frame(self.domain_zone, bg='steelblue')
            self.option_frame.grid(row=1, column=0)

            Label_struct_file = Label(self.option_frame, text='Please choose the structure file :',
                                      bg='steelblue', font=('microsoft yahei', 12, 'normal'))
            Label_struct_file.grid(row=0, column=0, sticky='w', padx=10)
            # Create the combobox to choose structure file
            self.struct_file_list = ttk.Combobox(self.option_frame, width=65, state='readonly')
            self.struct_file_list['values'] = [x for x in self.data_name if '.txt' in x]
            self.struct_file_list.bind("<<ComboboxSelected>>", lambda e: self.combobox_file())
            self.struct_file_list.grid(row=0, column=0, columnspan=2)

            # Create checkbutton to choose notion
            Label_notion = Label(self.option_frame, text='Please choose the structural notion :     '
                                                         '(Based on Swiss-Prot database)',
                                 bg='steelblue', font=('microsoft yahei', 12, 'normal'))
            Label_notion.grid(row=1, column=0, sticky='w', padx=10, pady=5)
            # Optional to filter structural result by a keyword
            synonym_label = Label(self.option_frame, text='Synonym of motif (ex. D-box) : ',
                                  bg='steelblue', font=('microsoft yahei', 12, 'normal'))
            synonym_label.grid(row=2, column=0, sticky='w', padx=10)

            self.keyword = StringVar()
            synonym_text = Entry(self.option_frame, textvariable=self.keyword, bg='white', width=40)
            synonym_text.grid(row=2, column=0, padx=10)

            # Frame to create checkbutton
            button_frame = Frame(self.option_frame, bg='steelblue')
            button_frame.grid(row=3, columnspan=2, sticky='w', padx=10)
            # Structural domain
            struct_domain = LabelFrame(button_frame, text='Structural domain', bg='steelblue',
                                       font=('microsoft yahei', 12, 'normal'))
            struct_domain.pack(side='left', pady=10)
            # Optional checkbutton
            optional_notion = LabelFrame(button_frame, text='Optional', bg='steelblue',
                                         font=('microsoft yahei', 12, 'normal'))
            optional_notion.pack(side='left', fill='y', pady=10, padx=10)
            # sub-frame for checkbutton
            self.notion_frame1 = Frame(struct_domain, bg='steelblue')
            self.notion_frame1.pack()
            self.notion_frame2 = Frame(optional_notion, bg='steelblue')
            self.notion_frame2.pack()

            self.create_checkbutton()

    def create_checkbutton(self):
        """
        Create checkbutton to choose interest notion
        :return: checkbutton frame
        """
        # feature notion list
        self.notion_list = ['ACT_SITE', 'BINDING', 'CARBOHYD', 'CA_BIND', 'COILED', 'DNA_BIND', 'DOMAIN', 'INTRAMEM',
                            'HELIX', 'LIPID', 'METAL', 'MOD_RES', 'MOTIF', 'NP_BIND', 'REGION',
                            'STRAND', 'TOPO_DOM', 'TRANSMEM', 'TURN', 'ZN_FING',
                            # optional
                            'CHAIN', 'COMPBIAS', 'CONFLICT', 'CROSSLNK', 'DISULFID', 'INIT_MET', 'MUTAGEN',
                            'NON_CONS', 'NON_STD', 'NON_TER', 'PEPTIDE', 'PROPEP', 'REPEAT', 'SIGNAL',
                            'SITE', 'TRANSIT',  'UNSURE', 'VARIANT', 'VAR_SEQ']
        # Create a dictionary to store the keys as checkbutton variable
        notion_dico = {}
        for i in range(len(self.notion_list)):
            notion_dico[i] = self.notion_list[i]
        # Dictionary to get the boolean value of each checkbutton
        self.checkbox = {}
        for i in range(len(self.notion_list)):
            self.checkbox[i] = tkinter.BooleanVar()
            # Change line when the checkbutton is out of window
            if i < 6:
                button = Checkbutton(self.notion_frame1, text=self.notion_list[i], bg='steelblue',
                                     variable=self.checkbox[i])
                button.grid(row=0, column=i, sticky='w')
            if 5 < i < 12:
                button = Checkbutton(self.notion_frame1, text=self.notion_list[i], bg='steelblue',
                                     variable=self.checkbox[i])
                button.grid(row=1, column=i-6, sticky='w')
            if 11 < i < 18:
                button = Checkbutton(self.notion_frame1, text=self.notion_list[i], bg='steelblue',
                                     variable=self.checkbox[i])
                button.grid(row=2, column=i-12, sticky='w')
            if 17 < i < 20:
                button = Checkbutton(self.notion_frame1, text=self.notion_list[i], bg='steelblue',
                                     variable=self.checkbox[i])
                button.grid(row=3, column=i-18, sticky='w')
            # Optional
            if 19 < i < 25:
                button = Checkbutton(self.notion_frame2, text=self.notion_list[i], bg='steelblue',
                                     variable=self.checkbox[i])
                button.grid(row=0, column=i-20, sticky='w')
            if 24 < i < 30:
                button = Checkbutton(self.notion_frame2, text=self.notion_list[i], bg='steelblue',
                                     variable=self.checkbox[i])
                button.grid(row=1, column=i-25, sticky='w')
            if 29 < i < 35:
                button = Checkbutton(self.notion_frame2, text=self.notion_list[i], bg='steelblue',
                                     variable=self.checkbox[i])
                button.grid(row=2, column=i-30, sticky='w')
            if 34 < i < 39:
                button = Checkbutton(self.notion_frame2, text=self.notion_list[i], bg='steelblue',
                                     variable=self.checkbox[i])
                button.grid(row=3, column=i-35, sticky='w')

    def send_checkbutton(self):
        """
        Store the chosen checkbutton when execute
        :return: chosen notion list
        """
        checklist = []

        for j in self.checkbox:
            if self.checkbox[j].get():
                checklist.append(self.notion_list[j])
        return checklist

    def find_sub_motif_frame(self):
        """
        Define the sub motif research program frame
        """
        # Reset the frame
        self.parameter_frame['text'] = 'Parameters'
        self.reset_parameter()
        self.program = 'find_sub_motif'
        self.program_title.set('Find sub motif')

        # Create the new attributes
        find_motif = Frame(self.parameter_childframe, bg='steelblue', width=700, height=400)
        find_motif.pack(fill='both')

        Label1 = Label(find_motif, text="Please entry the main motif :  ",
                       bg='steelblue', font=('microsoft yahei', 12, 'normal'))
        Label1.grid(row=0, column=0, sticky='w', pady=5, padx=10)

        self.input_motif = StringVar()
        entry_motif = Entry(find_motif, textvariable=self.input_motif, bg='white', width=62)
        entry_motif.grid(row=0, column=1, pady=5, padx=10)

        Label2 = Label(find_motif, text='Please choose the fasta file:', bg='steelblue',
                       font=('microsoft yahei', 12, 'normal'))
        Label2.grid(row=1, column=0, sticky='w', pady=5, padx=10)
        # Create the combobox to choose fasta file
        self.file_list = ttk.Combobox(find_motif, width=60, state='readonly')
        self.file_list['values'] = [x for x in self.data_name if '.fasta' in x]
        self.file_list.bind("<<ComboboxSelected>>", lambda e: self.combobox_file())
        self.file_list.grid(row=1, column=1)

        Label3 = Label(find_motif, text="Please entry the second motif :  ",
                       bg='steelblue', font=('microsoft yahei', 12, 'normal'))
        Label3.grid(row=2, column=0, sticky='w', pady=5, padx=10)

        self.input_sub_motif = StringVar()
        entry_sub_motif = Entry(find_motif, textvariable=self.input_sub_motif, bg='white', width=62)
        entry_sub_motif.grid(row=2, column=1, pady=5, padx=10)

        Label4 = Label(find_motif, text="Please define the distance between these motif :  ",
                       bg='steelblue', font=('microsoft yahei', 12, 'normal'))
        Label4.grid(row=3, column=0, sticky='w', pady=5, padx=10)

        Label5 = Label(find_motif, text="(Entry a whole number, ex. 20) ",
                       bg='steelblue', font=('microsoft yahei', 12, 'normal'))
        Label5.grid(row=4, column=0, sticky='w', pady=5, padx=10)

        self.distance_2_motif = StringVar()
        entry_sub_motif = Entry(find_motif, textvariable=self.distance_2_motif, bg='white', width=62)
        entry_sub_motif.grid(row=3, column=1, pady=5, padx=10)

        self.button = 'execute'
        Bt = Button(self.parameter_childframe, text='Execute', bg='lightsteelblue', width=15,
                    font=('microsoft yahei', 12, 'normal'),
                    command=lambda: [self.set_button_variable('execute'),
                                     self.controller.press_execute()])
                                     # self.loading_window(self.controller.press_execute)])

        Bt.pack(pady=5)

    def find_multiple_motif_frame(self):
        """
        Define the multiple motif research program frame
        """
        # Reset the frame
        self.parameter_frame['text'] = 'Parameters'
        self.reset_parameter()
        self.program = 'find_multiple_motif'
        self.program_title.set('Find multiple motifs')

        # Create the new attributes
        find_motif = Frame(self.parameter_childframe, bg='steelblue', width=700, height=400)
        find_motif.pack(fill='both')

        Label1 = Label(find_motif, text="Please entry your motif : (separate motifs with commas) ",
                       bg='steelblue', font=('microsoft yahei', 12, 'normal'))
        Label1.grid(row=0, column=0, sticky='w', pady=5, padx=10)

        self.input_motif = StringVar()
        entry_motif = Entry(find_motif, textvariable=self.input_motif, bg='white', width=62)
        entry_motif.grid(row=0, column=1, pady=5, padx=10)

        Label2 = Label(find_motif, text='Please choose the fasta file:', bg='steelblue',
                       font=('microsoft yahei', 12, 'normal'))
        Label2.grid(row=1, column=0, sticky='w', pady=5, padx=10)
        # Create the combobox to choose fasta file
        self.file_list = ttk.Combobox(find_motif, width=60, state='readonly')
        self.file_list['values'] = [x for x in self.data_name if '.fasta' in x]
        self.file_list.bind("<<ComboboxSelected>>", lambda e: self.combobox_file())
        self.file_list.grid(row=1, column=1)

        self.button = 'execute'
        Bt = Button(self.parameter_childframe, text='Execute', bg='lightsteelblue', width=15,
                    font=('microsoft yahei', 12, 'normal'),
                    command=lambda: [self.set_button_variable('execute'),
                                     # self.controller.press_execute()])
                                     self.loading_window(self.controller.press_execute)])

        Bt.pack(pady=5)

    def import_file(self):
        self.file_import = filedialog.askopenfilename(title='Import file',
                                                      filetypes=[('', '.txt'),
                                                                 ('', '*.fasta')])
        if self.file_import:
            new_file = Frame(self.document_tmp, bg='lightsteelblue', width=28)
            new_file.pack(fill='both', pady=5, padx=5, side='bottom')
            file_name = "Data_%s : " % self.n + self.file_import.split('/')[-1]
            file_label = ttk.Label(new_file, text=file_name, wraplength=185, width=28)
            file_label.grid(sticky='w', pady=5, columnspan=2)
            self.data_name.append(file_name)
            # Store the file path for the file
            self.path_dict[file_name] = self.file_import
            # Create button for each file frame
            self.n = self.n+1
            Button(new_file, text='Deleted',
                   command=lambda: self.delete_file_frame(new_file, file_name)).grid(row=1, column=1, sticky='e')
            # Refresh the frame to update the combobox
            if self.program == 'find_motif':
                self.find_motif_frame()
            elif self.program == 'find_sub_motif':
                self.find_sub_motif_frame()
            elif self.program == 'find_multiple_motif':
                self.find_multiple_motif_frame()

    def result_frame(self):
        """
        Create attribute for result in the history
        """
        if self.controller.program == 'find_motif':
            new_file = Frame(self.document_tmp, bg='lightsteelblue', width=32)
            new_file.pack(fill='both', pady=5, padx=5, side='bottom')
            file_name = "Data_%s : " % self.n + self.input_motif.get()+' in ' \
                        + self.file_list.get().split('.fasta')[0] + '.xlsx'
            file_label = ttk.Label(new_file, text=file_name, wraplength=185, width=32)
            file_label.grid(sticky='w', pady=5, columnspan=2)
            self.n = self.n + 1
            # Store the result to export
            df = self.controller.df
            stats = self.controller.stats
            chi2 = self.controller.chi2
            freq = self.controller.freq
            Button(new_file, text='Export',
                   command=lambda: [self.set_button_variable('export_base'),
                                    self.loading_window(self.controller.press_export_base,
                                                        args=(df, stats, chi2, freq))]
                   ).grid(row=1, column=0, sticky='e')
            Button(new_file, text='Deleted',
                   command=lambda: self.delete_file_frame(new_file, file_name)).grid(row=1, column=1, sticky='e')
            self.data_name.append(file_name)
            self.path_dict[file_name] = file_name

            # Create the second table for domain information
            if self.domain_table.get() == 1:
                new_file = Frame(self.document_tmp, bg='lightsteelblue', width=32)
                new_file.pack(fill='both', pady=5, padx=5, side='bottom')
                file_name = "Data_%s : " % self.n + self.input_motif.get() + ' in ' \
                            + self.file_list.get().split('.fasta')[0] + ' domain' + '.xlsx'
                file_label = ttk.Label(new_file, text=file_name, wraplength=185, width=32)
                file_label.grid(sticky='w', pady=5, columnspan=2)
                self.n = self.n + 1

                domain_df = self.controller.df1
                not_domain_df = self.controller.df2
                detail_df = self.controller.detail
                summary_df = self.controller.summary
                Button(new_file, text='Export',
                       command=lambda: [self.set_button_variable('export_detail'),
                                        self.loading_window(self.controller.press_export_detail,
                                                            args=(domain_df, not_domain_df, detail_df, summary_df))]
                       ).grid(row=1, column=0, sticky='e')
                Button(new_file, text='Deleted',
                       command=lambda: self.delete_file_frame(new_file, file_name)).grid(row=1, column=1, sticky='e')
                self.data_name.append(file_name)
                self.path_dict[file_name] = file_name

        elif self.controller.program == 'find_sub_motif':
            new_file = Frame(self.document_tmp, bg='lightsteelblue', width=32)
            new_file.pack(fill='both', pady=5, padx=5, side='bottom')
            file_name = "Data_%s : " % self.n + self.input_motif.get() + ' ' + self.input_sub_motif.get() + \
                        ' in ' + self.file_list.get().split('.fasta')[0] + '.xlsx'
            file_label = ttk.Label(new_file, text=file_name, wraplength=185, width=32)
            file_label.grid(sticky='w', pady=5, columnspan=2)
            self.n = self.n + 1
            # Store the result to export
            sub_df = self.controller.sub_df
            Button(new_file, text='Export',
                   command=lambda: [self.set_button_variable('export_sub'),
                                    # self.controller.press_export_sub_motif(sub_df)]
                                    self.loading_window(self.controller.press_export_sub_motif,
                                                        args=(sub_df,))]
                   ).grid(row=1, column=0, sticky='e')
            Button(new_file, text='Deleted',
                   command=lambda: self.delete_file_frame(new_file, file_name)).grid(row=1, column=1, sticky='e')
            self.data_name.append(file_name)
            self.path_dict[file_name] = file_name

        elif self.controller.program == 'find_multiple_motif':
            new_file = Frame(self.document_tmp, bg='lightsteelblue', width=32)
            new_file.pack(fill='both', pady=5, padx=5, side='bottom')
            file_name = "Data_%s : " % self.n + self.input_motif.get() + ' in ' \
                        + self.file_list.get().split('.fasta')[0] + '.xlsx'
            file_label = ttk.Label(new_file, text=file_name, wraplength=185, width=32)
            file_label.grid(sticky='w', pady=5, columnspan=2)
            self.n = self.n + 1
            # Store the result to export
            df1 = self.controller.multiple_df1
            df2 = self.controller.multiple_df2

            Button(new_file, text='Export',
                   command=lambda: [self.set_button_variable('export_multiple'),
                                    self.loading_window(self.controller.press_export_multiple_motif,
                                                        args=(df1, df2))]
                   ).grid(row=1, column=0, sticky='e')
            Button(new_file, text='Deleted',
                   command=lambda: self.delete_file_frame(new_file, file_name)).grid(row=1, column=1, sticky='e')
            self.data_name.append(file_name)
            self.path_dict[file_name] = file_name

    def set_button_variable(self, x):
        """ Set the variable for different button, this function is used for calling loading window """
        self.button = x

    def export(self):
        """
        Class to define the file name of export file
        """
        self.save_file = filedialog.asksaveasfile(mode='w', defaultextension=".xlsx")
        return self.save_file

    def start_compute(self):
        """
        Check if the fasta filepath is valid
        """
        if self.fasta_path != '':
            if self.controller.program == 'find_motif':
                if not self.controller.dico:
                    self.show_error()
                if self.controller.dico is False:
                    messagebox.showerror('Error', 'The path isn\'t valid.')
                elif self.controller.dico != '':
                    messagebox.showinfo('Info', 'Finished!')
            elif self.controller.program == 'find_sub_motif':
                if self.controller.prot_dico is False:
                    messagebox.showerror('Error', 'The path isn\'t valid.')
                elif self.controller.prot_dico != '':
                    messagebox.showinfo('Info', 'Finished!')
            elif self.controller.program == 'find_multiple_motif':
                if self.controller.prot_dico is False:
                    messagebox.showerror('Error', 'The path isn\'t valid.')
                elif self.controller.prot_dico != '':
                    messagebox.showinfo('Info', 'Finished!')

    def reset_history(self):
        """ Clear all of history """
        answer = messagebox.askyesno('Info', 'Are you sure to clear all the history ?')
        if answer:
            self.document_tmp.destroy()
            self.document_tmp = Frame(self.canvas, bg='steelblue', width=210, height=400)
            self.canvas.create_window((0, 0), window=self.document_tmp, anchor='nw')
            self.path_dict = {}
            self.data_name = []
            self.reset_parameter()
            self.create_homepage()
        if not answer:
            return False

    def delete_file_frame(self, frame, file):
        """ Delete the file from history and refresh the file list """
        try:
            self.data_name.remove(file)
        except ValueError:
            pass
        frame.destroy()

    def combobox_file(self):
        """ Return the fasta file path below the choice """
        if self.program == 'find_motif':
            try:
                self.fasta_path = self.path_dict[self.file_list.get()]
                if self.domain_table.get() == 0:
                    return self.fasta_path
                elif self.domain_table.get() == 1:
                    self.struct_path = self.path_dict[self.struct_file_list.get()]
                    return self.fasta_path, self.struct_path
            except KeyError:
                return False
        elif self.program == 'find_sub_motif':
            try:
                self.fasta_path = self.path_dict[self.file_list.get()]
                return self.fasta_path
            except KeyError:
                return False

        elif self.program == 'find_multiple_motif':
            try:
                self.fasta_path = self.path_dict[self.file_list.get()]
                return self.fasta_path
            except KeyError:
                return False

    def show_msg(self):
        """ Show success messenger """
        if self.controller.save == 'ok':
            messagebox.showinfo('Info', 'Saved successfully!')
        else:
            pass

    def check_input(self):
        if self.controller.program == 'find_motif':
            input_result = self.input_motif.get().replace(' ','')
            if input_result.isalpha() is False:
                messagebox.showerror('Error', 'The motif is invalid, please re-entry.')
                return False
        elif self.controller.program == 'find_sub_motif':
            input_motif = self.input_motif.get().replace(' ', '')
            input_sub_motif = self.input_sub_motif.get().replace(' ', '')
            if input_motif.isalpha() is False or input_sub_motif is False:
                messagebox.showerror('Error', 'The motif is invalid, please re-entry.')
                return False
        elif self.controller.program == 'find_multiple_motif':
            input_motif = self.input_motif.get().replace(' ', '').replace(',','')
            print(input_motif)
            if input_motif.isalpha() is False:
                messagebox.showerror('Error', 'The motif is invalid, please re-entry.')
                return False

    def show_error(self):
        """ Show error messenger for different situation """
        if self.controller.program == 'find_motif':
            if not self.controller.motif and not self.controller.path_result:
                messagebox.showerror('Error', 'The champ is empty!')
            elif not self.controller.motif:
                messagebox.showerror('Error', 'Please entry the motif.')
            elif not self.controller.path_result:
                messagebox.showerror('Error', 'Please choose the file.')
            elif self.controller.check_list == []:
                messagebox.showerror('Error', 'Please choose at least one.')
        elif self.controller.program == 'find_sub_motif':
            if not self.controller.motif and not self.controller.path_result:
                messagebox.showerror('Error', 'The champ is empty!')
            elif not self.controller.motif:
                messagebox.showerror('Error', 'Please entry the motif.')
            elif not self.controller.path_result:
                messagebox.showerror('Error', 'Please choose the file.')
            elif not self.controller.sub_motif:
                messagebox.showerror('Error', 'Please entry the second motif.')
            if self.controller.num_invalid == 'yes':
                messagebox.showerror('Error', 'Please entry a whole number.')
        elif self.controller.program == 'find_multiple_motif':
            if not self.controller.motif and not self.controller.path_result:
                messagebox.showerror('Error', 'The champ is empty!')
            elif not self.controller.motif:
                messagebox.showerror('Error', 'Please entry the motif.')
            elif not self.controller.path_result:
                messagebox.showerror('Error', 'Please choose the file.')

    def loading_window(self, func, args=None):
        """ create loading window when processing different program"""
        if self.button == 'execute':
            t = Thread(target=func, daemon=True)
            t.start()
        elif self.button == 'export_base':
            t = Thread(target=func, args=args, daemon=True)
            t.start()
        elif self.button == 'export_detail':
            t = Thread(target=func, args=args, daemon=True)
            t.start()
        elif self.button == 'export_sub':
            t = Thread(target=func, args=args, daemon=True)
            t.start()
        elif self.button == 'export_multiple':
            t = Thread(target=func, args=args, daemon=True)
            t.start()

        # Create the loading screen
        loading_screen = Toplevel(self.window)
        loading_screen.title('Progress')
        loading_label = Label(loading_screen, text="Loading... ...\n\nPlease wait... ...",
                              width=50)
        loading_label.pack()
        center_window(loading_screen, 300, 90)

        # While the thread is alive
        while t.is_alive():
            # Update the window so it will keep responding
            self.window.update()

        loading_screen.destroy()
        self.window.deiconify()
        self.window.focus_force()


def get_screen_size(window):
    """ Return the size of screen. """
    return window.winfo_screenwidth(), window.winfo_screenheight()


def center_window(window, width, height):
    """
    Show the interface at the centre of screen.
    """
    screenwidth = window.winfo_screenwidth()
    screenheight = window.winfo_screenheight()
    size = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height-50) / 2)
    window.geometry(size)


def open_url(event):
    webbrowser.open("https://gitlab.com/juejun14/motifind", new=0)
