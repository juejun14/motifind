# -*- coding: utf-8 -*-
# @Time : 2022/5/28 23:45
# @Author : Juejun CHEN

"""
This file is used to run the controller
"""

from controller import Controller

if __name__ == "__main__":
    controller = Controller()
    controller.start_view()

