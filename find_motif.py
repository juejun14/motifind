# -*- coding: utf-8 -*-
# @Time : 2022/5/05 23:45
# @Author : Juejun CHEN

import re
import pandas as pd
import math
import collections
from openpyxl.workbook import Workbook
import numpy as np
from scipy.stats import chi2, chi2_contingency


class Motif:
    """
    Class to research motif from a fasta file and return an excel table
    """
    def __init__(self):
        self.columns_name = ['Protein', 'Description', 'Motif', 'Length']
        self.df = pd.DataFrame(columns=self.columns_name)
        self.nb_motif_dico = {}
        self.nb_motif = []
        self.to_df = {}
        self.length = {}
        self.prot_name_list = {}
        self.motif = ''
        self.theo_motif = collections.defaultdict(list)
        self.obs_dico = {}
        self.theory = 1

    def readfile(self, file_path, radiobutton=None):
        """
        Transform the sequence file to dictionary

        :param file_path: protein sequence file
        :return: The dictionary of the import protein sequence
        """
        # file = file_path
        self.prot_dico = dict()
        self.freq_dict = {}
        list_tmp = []
        key, value, all_letter = '', '', ''

        try:
            with open(file_path, 'r') as f:
                line = f.readline()
                while line:
                    # Set the first line as key
                    if '>' in line:
                        line = line.replace('\n', '')
                        prot = line.split('|')[1]
                        prot_name = line.split('|')[2]
                        key = prot
                        self.prot_name_list[key] = prot_name
                        list_tmp = []
                        value = ''
                    # Set the next rows as value of the key
                    elif '>' not in line:
                        line = line.replace('\n', '')
                        list_tmp.append(line)
                        seq = value.join(list_tmp)
                        self.prot_dico[key] = seq
                    line = f.readline()

                # Count the length of each sequence
                # Generate the frequency of all sequence
                for key, value in self.prot_dico.items():
                    self.length[key] = len(value)
                    if radiobutton == 1:
                        freq_tmp = self.frequency(value)
                        for k, v in freq_tmp.items():
                            if k in self.freq_dict.keys():
                                self.freq_dict[k] = self.freq_dict[k] + v
                            else:
                                self.freq_dict[k] = v
                    else:
                        pass
            return self.prot_dico, self.length

        except:
            return False

    def frequency(self, sequence):
        """
        Function to generate the amino acid  frequency dictionary.
        """
        # Define the frequency element as a dictionary
        self.freq = {}
        # Count each element of sequence and add element as the key, add frequency as value
        for element in sequence:
            self.freq[element] = sequence.count(element)
        return self.freq

    def compute_acid_proba(self):
        """
        Compute the probability of amino acid  from the fasta file
        """
        self.acid = {}
        self.total_length = 0
        # Count the number of each amino acid
        for v in self.length.values():
            self.total_length = self.total_length + v
        # Calculate the probability from the total length
        for key, value in self.freq_dict.items():
            self.acid[key] = value/self.total_length
        return self.acid

    def find_motif(self, motif, dictionary):
        """ Find the motif and return the position of sequence"""
        self.motif = motif.replace('X', '')
        motif = motif.replace('X', '[A-Z]')
        test = re.compile(motif)
        for key, value in dictionary.items():
            # Find motif
            found = False
            result = re.finditer(test, value)
            # If found motif in sequence
            for i in result:
                # Return motif
                found = True
                if key in self.nb_motif_dico.keys():
                    self.to_df[key] += [i.group(), i.span()]
                    self.nb_motif_dico[key] = 1 + self.nb_motif_dico[key]
                else:
                    self.to_df[key] = [i.group(), i.span()]
                    self.nb_motif_dico[key] = 1
            # If not found motif in sequence
            if not found:
                self.to_df[key] = None
                self.nb_motif_dico[key] = 0
        self.compute_acid_proba()
        return self.to_df

    def computer_proba(self, radiobutton):
        """ Calculate the probability of specified motif """
        self.theo_motif, self.theory_dico = {}, {}
        self.p = []

        if radiobutton == 0:
            self.theory = math.pow(1/20, len(self.motif))

        elif radiobutton == 1:
            for i in self.motif:
                if i in self.acid.keys():
                    self.p.append(self.acid[i])
            for i in self.p:
                self.theory = i * self.theory
        # Calculate the theory number of motif
        for key, value in self.prot_dico.items():
            self.theo_motif[key] = self.theory * len(value)
            self.theory_dico[key] = self.theory
        # Calculate the observed probability of motif
        for key, value in self.prot_dico.items():
            self.obs_dico[key] = self.nb_motif_dico[key]/self.length[key]
        return self.theo_motif, self.obs_dico

    def create_ratio(self):
        """ Compute the ratio value """
        self.ratio, self.ratio_log2 = {}, {}
        for k in self.theo_motif.keys():
            if self.obs_dico[k] == 0:
                self.ratio[k] = 0
                self.ratio_log2[k] = 0
            else:
                self.ratio[k] = (self.obs_dico[k]/self.theory)
                self.ratio_log2[k] = np.log2(self.ratio[k])

    def correction_log(self):
        """ Function for log2 transformation """
        self.log2_length = {}
        for key , value in self.length.items():
            self.log2_length[key] = np.log2(value)

        self.log2_motif = {}
        for key , value in self.nb_motif_dico.items():
            if value == 0:
                self.log2_motif[key] = 0
            else:
                self.log2_motif[key] = np.log2(value)

        self.log2_theo = {}
        for key, value in self.theo_motif.items():
            self.log2_theo[key] = np.log2(value)

        self.log2_obs = {}
        for key, value in self.obs_dico.items():
            if value == 0:
                self.log2_obs[key] = 0
            else:
                self.log2_obs[key] = np.log2(value)

    def insert_to_df(self, column, dico):
        """ Insert a new column to dataframe"""
        self.df[column] = dico.values()
        return self.df

    def create_dataframe(self):
        """
        Create empty table and Insert the data into table by columns
        """
        self.df['Protein'] = self.to_df.keys()
        self.df.set_index('Protein', inplace=True)

        self.name = ['Description', 'Motif', 'Length', 'log2_length', 'nb_motif',
                     'log2_motif', 'Theory_motif', 'log2_theo', 'Theory_proba',
                     'Obs', 'log2_obs', 'ratio', 'log2_ratio']
        self.dictionary = [self.prot_name_list, self.to_df, self.length, self.log2_length,
                           self.nb_motif_dico, self.log2_motif,  self.theo_motif,
                           self.log2_theo, self.theory_dico, self.obs_dico, self.log2_obs,
                           self.ratio, self.ratio_log2]
        for i in range(len(self.name)):
            self.insert_to_df(self.name[i], self.dictionary[i])

        return self.df

    def delete_zero(self):
        """ Drop the line without motif found"""
        self.df = self.df[~ self.df['nb_motif'].isin([0])]
        return self.df

    def stats_table(self):
        """
        Function for chi2 test
        """
        self.stats = self.df.describe().round(5)

        nb_obs_obs = np.sum(list(map(lambda x: x >= 1, list(self.nb_motif_dico.values()))))
        nb_obs_not_obs = np.sum(list(map(lambda x: x < 1, list(self.nb_motif_dico.values()))))
        nb_theory_obs = np.sum(list(map(lambda x: x >= 1, list(self.theo_motif.values()))))
        nb_theory_not_obs = np.sum(list(map(lambda x: x < 1, list(self.theo_motif.values()))))

        self.chi2 = [[nb_obs_obs, nb_theory_obs], [nb_obs_not_obs, nb_theory_not_obs]]
        try:
            stat, p, dof, expected = chi2_contingency(self.chi2)
            result_list = [stat, p, dof]

            prob = 0.95
            critical = chi2.ppf(prob, dof)
            result_list.append(prob)
            result_list.append(critical)
            if abs(stat) >= critical:
                result = 'Reject H0:Dependent'
            else:
                result = 'Fail to reject H0:Independent'
            result_list.append(result)

            # Make the decision of the chi-square test
            alpha = 1.0 - prob
            result_list.append(alpha)
            if p <= alpha:
                conclusion = 'Dependent (reject H0)'
            else:
                conclusion = 'Independent (fail to reject H0)'
            result_list.append(conclusion)

            # Create dataframe to store the statistic table
            index = ['nb_obs', 'nb_not_obs']
            self.chi2_df = pd.DataFrame(columns=('index', 'Observed', 'Theory'))
            self.chi2_df['index'] = index
            self.chi2_df.set_index(['index'], inplace=True)
            self.chi2_df['Observed'] = [nb_obs_obs, nb_obs_not_obs]
            self.chi2_df['Theory'] = [nb_theory_obs, nb_theory_not_obs]

            self.excepted_df = pd.DataFrame(expected, columns=('Observed', 'Theory'))
            self.excepted_df.insert(0, 'index', index)
            self.excepted_df.set_index(['index'], inplace=True)

            result_name = ['Result']
            self.result_df = pd.DataFrame(columns=result_name)
            self.result_df['Result'] = result_list
            col_name = ['Chi2', 'p-value', 'dof', 'probability', 'critical', 'test-statistic',
                        'significance', 'p-value decision']
            self.result_df.insert(0, 'index', [i for i in col_name])
            self.result_df.set_index(['index'], inplace=True)

            self.chi2_df = pd.concat([self.chi2_df, self.excepted_df, self.result_df],
                                     keys=('Observed', 'Expected', 'Conclusion'))

            self.freq_acid = pd.DataFrame(columns=('amino_acid', 'probability'))
            self.freq_acid['amino_acid'] = self.acid.keys()
            self.freq_acid.set_index(['amino_acid'], inplace=True)
            self.freq_acid['probability'] = self.acid.values()

            return self.stats, self.chi2_df, self.freq_acid

        except ValueError:
            # When there is 0 element, print only frequency table
            self.freq_acid = pd.DataFrame(columns=('amino_acid', 'probability'))
            self.freq_acid['amino_acid'] = self.acid.keys()
            self.freq_acid.set_index(['amino_acid'], inplace=True)
            self.freq_acid['probability'] = self.acid.values()

            return self.freq_acid

    def export(self, filename, frame, df, stats, chi2, freq):
        """
        Export all dataframe into the excel table
        """
        if frame == 'find_motif':
            df.to_excel(filename, index=True)
            with pd.ExcelWriter(filename) as writer:
                df.to_excel(writer, sheet_name='main_table')
                try:
                    stats.to_excel(writer, sheet_name='statistic_table')
                    chi2.to_excel(writer, sheet_name='chi2_table')
                except AttributeError:
                    # When 0 motif found
                    pass
                freq.to_excel(writer, sheet_name='freq')
            self.save = 'ok'
        else:
            self.save = 'no'


