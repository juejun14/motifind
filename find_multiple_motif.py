# -*- coding: utf-8 -*-
# @Time : 2022/5/25 2:15
# @Author : Juejun CHEN
import collections
import re

import pandas as pd
import numpy as np


class FindMultipleMotif:
    """
    Search multiple motif at once
    """
    def __init__(self):
        self.prot_name_list = {}
        self.length = {}
        self.nb_motif_dico = {}
        self.to_df = {}

    def readfile(self, file_path, radiobutton=None):
        """
        Transform the sequence file to dictionary

        :param file_path: protein sequence file
        :return: The dictionary of the import protein sequence
        """
        # file = file_path
        self.prot_dico = dict()
        self.freq_dict = {}
        list_tmp = []
        key, value, all_letter = '', '', ''

        try:
            with open(file_path, 'r') as f:
                line = f.readline()
                while line:
                    # Set the first line as key
                    if '>' in line:
                        line = line.replace('\n', '')
                        prot = line.split('|')[1]
                        prot_name = line.split('|')[2]
                        key = prot
                        self.prot_name_list[key] = prot_name
                        list_tmp = []
                        value = ''
                    # Set the next rows as value of the key
                    elif '>' not in line:
                        line = line.replace('\n', '')
                        list_tmp.append(line)
                        seq = value.join(list_tmp)
                        self.prot_dico[key] = seq
                    line = f.readline()

            return self.prot_dico, self.length

        except:
            return False

    def frequency(self, sequence):
        """
        Function to generate the amino acid  frequency dictionary.
        """
        # Define the frequency element as a dictionary
        self.freq = {}
        # Count each element of sequence and add element as the key, add frequency as value
        for element in sequence:
            self.freq[element] = sequence.count(element)
        return self.freq

    def compute_acid_proba(self):
        self.acid = {}
        self.total_length = 0
        for v in self.length.values():
            self.total_length = self.total_length + v
        for key, value in self.freq_dict.items():
            self.acid[key] = value / self.total_length
        return self.acid

    def multiple_motif(self, motif):
        self.motif_list = []
        self.motif_list = motif.replace(' ', '').split(',')
        return self.motif_list

    def find_motif(self, motif, dictionary):
        """ Find the motif and return the position of sequence"""
        self.motif = motif.replace('X', '')
        motif = motif.replace('X', '[A-Z]')
        test = re.compile(motif)
        self.to_df = collections.defaultdict(list)
        self.nb_motif_dico = {}

        for key, value in dictionary.items():
            # Find motif
            found = False
            result = re.finditer(test, value)
            # If found motif in sequence
            for i in result:
                # Return motif
                found = True
                if key in self.nb_motif_dico.keys():
                    self.to_df[key] += [i.group(), i.span()]
                    self.nb_motif_dico[key] = 1 + self.nb_motif_dico[key]
                else:
                    self.to_df[key] = [i.group(), i.span()]
                    self.nb_motif_dico[key] = 1

            # If not found motif in sequence
            if not found:
                self.to_df[key] = None
                self.nb_motif_dico[key] = 0
        return self.to_df, self.nb_motif_dico

    def find_each_motif(self, motif_list, prot_dico):
        result_dico, result_nb_dico = {}, {}
        for motif in motif_list:
            result, nb = self.find_motif(motif, prot_dico)
            result_dico[motif] = result
            result_nb_dico[motif] = nb
        return result_dico, result_nb_dico

    def create_df(self, result_dico, motif_nb_dico):
        number_motif = len(result_dico)
        col_name = []
        for i in range(1, number_motif+1):
            col_name.append('Motif_%s' % i)

        multiple_motif_df = pd.DataFrame()
        multiple_motif_df['Protein'] = self.to_df.keys()
        multiple_motif_df.set_index('Protein', inplace=True)
        # for i in col_name:
        #     multiple_motif_df[i] = None
        for motif in self.motif_list:
            multiple_motif_df[motif] = result_dico[motif].values()
            multiple_motif_df['%s_nb' % motif] = motif_nb_dico[motif].values()

        # multiple_motif_df = multiple_motif_df[~ multiple_motif_df['nb_motif'].isin([0])]
        multiple_motif_df_without_none = multiple_motif_df.dropna(axis=0, how='any', inplace=False)

        return multiple_motif_df, multiple_motif_df_without_none

    def export_df(self, filename, df1, df2):
        if filename:
            df1.to_excel(filename, index=True)
            with pd.ExcelWriter(filename) as writer:
                df1.to_excel(writer, sheet_name='all_protein')
                df2.to_excel(writer, sheet_name='multiple_motif')
            self.save = 'ok'
        else:
            self.save = 'no'

