import pandas as pd
from Structure_find import *


class FindDatabase:
    """
    Class to extract specified column of an excel table and find if the same index exist in another motif tableau
    """
    def __init__(self):
        # Define the dataframe
        FindDomain.__init__(self)
        self.database = pd.DataFrame()

    def read_database(self, database, sheetname):
        """
        transform the excel file into dataframe
        :param database: absolute path of file
        :param sheetname: sheet name of excel table
        :return: the dataframe
        """
        self.database = pd.read_excel(database, index_col=0, sheet_name=sheetname)
        # print(self.database['SUB_AC'])
        return self.database

    def import_list(self, prot_df, sheetname, colname):
        """
        Extract the specified content from the table

        :param prot_df:  absolute path of proteome file
        :param sheetname: sheet name of excel table
        :return: A dataframe with protein information and a motif dictionary with interesting proteins
        """
        # All proteome result table
        self.prot_df = pd.read_excel(prot_df, index_col=0, sheet_name=sheetname)
        # Create a new dataframe
        self.filter_df = pd.DataFrame(columns=self.prot_df.columns)
        # Select the proteins in common and add to new dataframe
        for id in self.database[colname]:
            if id in self.prot_df.index:
                self.filter_df.loc[id] = self.prot_df.loc[id]
            else:
                pass
        # Create a dictionary to store the protein code and its motifs
        self.motif_dict = self.filter_df['Motif'].to_dict()
        return self.filter_df, self.motif_dict

    def export(self, filename, dataframe):
        """
        Export the result into an excel table
        """
        dataframe.to_excel(filename, index=True)
        with pd.ExcelWriter(filename) as writer:
            dataframe.to_excel(writer, sheet_name='main_table')


