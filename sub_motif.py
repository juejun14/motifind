# -*- coding: utf-8 -*-
# @Time : 2022/5/25 0:47
# @Author : Juejun CHEN

from find_motif import *


class FindSupMotif:
    """
    Search for second motif B from the defined length succeeding motif A found
    """
    def __init__(self):
        self.prot_name_list = {}
        self.length = {}
        self.nb_motif_dico = {}
        self.to_df = {}

    def readfile(self, file_path, radiobutton=None):
        """
        Transform the sequence file to dictionary

        :param file_path: protein sequence file
        :return: The dictionary of the import protein sequence
        """
        # file = file_path
        self.prot_dico = dict()
        self.freq_dict = {}
        list_tmp = []
        key, value, all_letter = '', '', ''

        try:
            with open(file_path, 'r') as f:
                line = f.readline()
                while line:
                    # Set the first line as key
                    if '>' in line:
                        line = line.replace('\n', '')
                        prot = line.split('|')[1]
                        prot_name = line.split('|')[2]
                        key = prot
                        self.prot_name_list[key] = prot_name
                        list_tmp = []
                        value = ''
                    # Set the next rows as value of the key
                    elif '>' not in line:
                        line = line.replace('\n', '')
                        list_tmp.append(line)
                        seq = value.join(list_tmp)
                        self.prot_dico[key] = seq
                    line = f.readline()

            return self.prot_dico, self.length

        except:
            return False

    def find_main_motif(self, motif, dictionary):
        """ Find the motif and return the position of sequence"""
        # self.motif = motif.replace('X', '')
        motif = motif.replace('X', '[A-Z]')
        motif_to_find = re.compile(motif)
        for key, value in dictionary.items():
            # Find motif
            found = False
            result = re.finditer(motif_to_find, value)
            # If found motif in sequence
            for i in result:
                # Return motif
                found = True
                if key in self.nb_motif_dico.keys():
                    self.to_df[key] += [i.group(), i.span()]
                    self.nb_motif_dico[key] = 1 + self.nb_motif_dico[key]
                else:
                    self.to_df[key] = [i.group(), i.span()]
                    self.nb_motif_dico[key] = 1
            # If not found motif in sequence
            if not found:
                self.to_df[key] = None
                self.nb_motif_dico[key] = 0
        # self.compute_acid_proba()
        return self.to_df

    def sup_motif(self, second_motif, zone_length=None):
        """ Create dataframe to store results """
        motif2 = second_motif
        motif2 = motif2.replace('X', '[A-Z]')
        motif2_to_find = re.compile(motif2)
        self.double_motif_df = pd.DataFrame(columns=('Protein', 'Motif1', 'Location', 'Motif2', 'Start location'),
                                            index=[])
        self.double_motif_df.index = np.arange(1, len(self.double_motif_df))

        for k, v in self.to_df.items():
            if not v:
                pass
            else:
                for element in v:
                    if type(element) == tuple:
                        if zone_length is None:
                            # find second motif from the end of the main motif
                            result = re.finditer(motif2_to_find, self.prot_dico[k][element[1]:])
                            for i in result:
                                if i.group() in self.prot_dico[k]:
                                    motif2_location = self.prot_dico[k].find(i.group())
                                    element_list = [k, v[v.index(element) - 1], element, i.group(),
                                                    motif2_location]
                                    index_size = self.double_motif_df.index.size + 1
                                    self.double_motif_df.loc[index_size] = element_list

                        elif type(zone_length) == int:
                            # find second motif from the end of the main motif
                            result = re.finditer(motif2_to_find, self.prot_dico[k][element[1]:element[1] + zone_length])
                            for i in result:
                                zone = self.prot_dico[k][element[1]:element[1] + zone_length]
                                if i.group() in self.prot_dico[k]:
                                    motif2_location = zone.find(i.group())
                                    element_list = [k, v[v.index(element)-1], element, i.group(),
                                                    element[1] + motif2_location]
                                    index_size = self.double_motif_df.index.size + 1
                                    self.double_motif_df.loc[index_size] = element_list
        print(self.double_motif_df)
        return self.double_motif_df

    def export_df(self, filename, df):
        if filename:
            df.to_excel(filename, index=True)
            self.save = 'ok'
        else:
            self.save = 'no'

